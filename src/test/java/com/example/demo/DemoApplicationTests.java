package com.example.demo;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fit.bi.tjv.controller.IslandController;
import cz.cvut.fit.bi.tjv.model.Island;
import cz.cvut.fit.bi.tjv.model.PirateGroup;
import cz.cvut.fit.bi.tjv.model.Ship;
import cz.cvut.fit.bi.tjv.service.IslandService;
import cz.cvut.fit.bi.tjv.service.PirateGroupService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static net.bytebuddy.matcher.ElementMatchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.mock.http.server.reactive.MockServerHttpRequest.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
//@WebMvcTest(IslandController.class)
@SpringBootTest
@AutoConfigureMockMvc
@ContextConfiguration
        (
                {
                        "file:src/main/resources/spring.xml"
                }
        )
public class DemoApplicationTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private IslandService islandService;

    @MockBean
    private PirateGroupService pirateGroupService;



    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    @Test
    public void givenIslands_whenGetIslands_thenReturnJsonArray()
            throws Exception {

        Island Ostruvek = new Island(1, "Ostruvek");

        List<Island> allIslands = Arrays.asList(Ostruvek);

        given(islandService.listAll()).willReturn(allIslands);

        mvc.perform(get("/islands")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name").value(Ostruvek.getName()))
                .andExpect(jsonPath("$[0].id_island").value(Ostruvek.getId_island()))
        ;
    }

    @Test
    public void givenIslandsWithRelationship_whenGetIslandsWithRelationship_thenReturnJsonArrayOfPirateGroups()
            throws Exception {

        Island Ostruvek = new Island(1, "Ostruvek");
        PirateGroup  passionatedGunners = new PirateGroup(1,"Passionated Gunners" , 0);
        PirateGroup losers = new PirateGroup(2, "Losers" , 150);

        List<Island> allIslands = Arrays.asList(Ostruvek);
        Set<PirateGroup> allPirateGroups = new HashSet<>(Arrays.asList(passionatedGunners, losers));
        Ostruvek.setPirateGroups(allPirateGroups);

        given(islandService.find(1)).willReturn(Ostruvek);

        mvc.perform(get("/islands/1/pirateGroups")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
        ;
    }

    @Test
    public void createNewIslandAndAddToPirateGroup() throws Exception
    {
        given(pirateGroupService.find(1)).willReturn(new PirateGroup("MyPirateGroup" , 564));
        mvc.perform( MockMvcRequestBuilders
                .post("/pirateGroups/1/islands")
                .content(asJsonString(new Island(1 , "Ostrov")))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andDo(print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id_island").exists());
    }

    @Test
    public void updateIsland() throws Exception
    {
        Island ostruvek = new Island(1, "ostruvek");
        mvc.perform( MockMvcRequestBuilders
                .put("/islands/1")
                .content(asJsonString(ostruvek))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id_island").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("ostruvek"));
    }

    @Test
    public void deleteIsland() throws Exception
    {
        mvc.perform( MockMvcRequestBuilders.delete("/islands/{id}", 1) )
                .andExpect(status().isAccepted());
    }

    @Test
    public void listShipsOfPirateGroup()
            throws Exception {

        PirateGroup  passionatedGunners = new PirateGroup(1,"Passionated Gunners" , 0);
        Ship ship1 = new Ship(1, "Black Pearl1", false);
        Ship ship2 = new Ship(2, "Black Pearl2", false);
        Ship ship3 = new Ship(3, "Black Pearl3", false);

        Set<Ship> allShips = new HashSet<>(Arrays.asList(ship1, ship2, ship3));
        passionatedGunners.setShips(allShips);

        given(pirateGroupService.find(1)).willReturn(passionatedGunners);

        mvc.perform(get("/pirateGroups/1/ships")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
        ;
    }


    @Test
    public void getPirateGroupOfShip()
            throws Exception {

        PirateGroup  passionatedGunners = new PirateGroup(1,"Passionated Gunners" , 0);
        Ship ship1 = new Ship(1, "Black Pearl1", false);
        Ship ship2 = new Ship(2, "Black Pearl2", false);
        Ship ship3 = new Ship(3, "Black Pearl3", false);

        Set<Ship> allShips = new HashSet<>(Arrays.asList(ship1, ship2, ship3));
        passionatedGunners.setShips(allShips);

        given(pirateGroupService.find(1)).willReturn(passionatedGunners);
        given(pirateGroupService.listAll()).willReturn(Arrays.asList(passionatedGunners));

        mvc.perform(get("/ships/1/pirateGroup")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id_pirategroup").value(1))
                .andExpect(jsonPath("$.name").value("Passionated Gunners"))
        ;
    }


}


