package cz.cvut.fit.bi.tjv.controller;

import cz.cvut.fit.bi.tjv.model.PirateGroup;
import cz.cvut.fit.bi.tjv.model.Ship;
import cz.cvut.fit.bi.tjv.service.PirateGroupService;
import cz.cvut.fit.bi.tjv.service.ShipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ShipController {
    @Autowired
    public ShipService shipService;
    @Autowired
    public PirateGroupService pirateGroupService;

    @GetMapping("/ships/{id}/pirateGroup")
    public PirateGroup getPirateGroup(@PathVariable String id) {
        for (PirateGroup group : pirateGroupService.listAll()) {
            for (Ship ship : group.getShips()) {
                if (ship.getId() == Integer.parseInt(id)){
                    return group;
                }

            }
        }
        return null;
    }

    @PostMapping(value = "/pirateGroups/{pirateGroupId}/ships")
    public ResponseEntity<Ship> addShip(@RequestBody Ship ship, @PathVariable String pirateGroupId) {
        pirateGroupService.find(Integer.parseInt(pirateGroupId)).addShip(ship);
    return new ResponseEntity<Ship>(ship, HttpStatus.CREATED);
    }


    @PutMapping(value = "/ships/{id}/")
    public ResponseEntity<Ship> updateShip(@RequestBody Ship ship, @PathVariable String id) {
        ship.setId(Integer.parseInt(id));
        shipService.addOrReplace(ship);
    return new ResponseEntity<Ship>(ship, HttpStatus.OK);
    }


    @DeleteMapping(value = "/ships/{id}")
    public ResponseEntity deleteShip(@PathVariable String id) {
        pirateGroupService.deleteGroup(Integer.parseInt(id));
        return new ResponseEntity(HttpStatus.ACCEPTED) ;
    }


    @GetMapping("/ships")
    public List<Ship> getShips() {
        //System.out.println("in get method");
        final List res = new ArrayList<Ship>();
        shipService.listAll().forEach(res::add);
        return res;

    }
}