package cz.cvut.fit.bi.tjv.dao;

import cz.cvut.fit.bi.tjv.model.Island;
import org.springframework.context.annotation.Bean;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IslandDao extends CrudRepository<Island, Integer> {

}