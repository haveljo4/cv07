package cz.cvut.fit.bi.tjv.service;


import cz.cvut.fit.bi.tjv.dao.PirateGroupDao;
import cz.cvut.fit.bi.tjv.model.PirateGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Service
public class PirateGroupService {
    @Autowired
    private PirateGroupDao groupDao;

    @Transactional
    public void addOrReplace(PirateGroup group) {
        groupDao.save(group);
    }

    @Transactional
    public Iterable<PirateGroup> listAll() {
        return  groupDao.findAll();
    }


    @Transactional
    public PirateGroup find(final Integer id) {
        return groupDao.findById(id).get();
    }


    @Transactional
    public void deleteGroup(final Integer id) {
        groupDao.deleteById(id);
    }


    @Transactional
    public void addAll(Collection<PirateGroup> groups) {
        for (PirateGroup group : groups) {
            groupDao.save(group);
        }
    }

//    @Transactional(readOnly = true)
//    public List<PirateGroup> listAll() {
//        return groupDao.findAll();
//    }
}