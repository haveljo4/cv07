package cz.cvut.fit.bi.tjv.dao;

import cz.cvut.fit.bi.tjv.model.Ship;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShipDao extends CrudRepository<Ship, Integer> {

}