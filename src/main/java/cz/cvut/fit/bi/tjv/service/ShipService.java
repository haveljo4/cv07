package cz.cvut.fit.bi.tjv.service;

import cz.cvut.fit.bi.tjv.dao.ShipDao;
import cz.cvut.fit.bi.tjv.model.Ship;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Service
public class ShipService {
    private ShipDao shipDao;

    @Autowired
    public ShipService(ShipDao shipDao) {
        this.shipDao = shipDao;
    }

    @Transactional
    public void add(Ship ship) {
        shipDao.save(ship);
    }

    @Transactional
    public void addAll(Collection<Ship> ships) {
        for (Ship product : ships) {
            shipDao.save(product);
        }
    }

    @Transactional(readOnly = true)
    public Iterable<Ship> listAll() {
        return shipDao.findAll();
    }


    @Transactional
    public Ship find(final Integer id) {
        return shipDao.findById(id).get();
    }



    @Transactional
    public void addOrReplace(Ship ship) {
        shipDao.save(ship);
    }

}