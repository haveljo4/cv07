package cz.cvut.fit.bi.tjv.service;


import cz.cvut.fit.bi.tjv.dao.IslandDao;
import cz.cvut.fit.bi.tjv.model.Island;
import cz.cvut.fit.bi.tjv.model.PirateGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Service
public class IslandService {
    @Autowired
    private IslandDao islandDao;

    @Transactional
    public void addOrReplace(Island island) {
        islandDao.save(island);
    }

    @Transactional
    public Island find(Integer id) {
        return islandDao.findById(id).get();
    }

    @Transactional
    public void deleteIsland(final Integer id) {
         islandDao.deleteById(id);
    }

    @Transactional
    public void addAll(Collection<Island> islands) {
        for (Island island : islands) {
            islandDao.save(island);
        }
    }



    @Transactional(readOnly = true)
    public Iterable<Island> listAll() {
        return islandDao.findAll();
    }
}