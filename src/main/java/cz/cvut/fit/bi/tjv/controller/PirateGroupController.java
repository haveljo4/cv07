package cz.cvut.fit.bi.tjv.controller;

import cz.cvut.fit.bi.tjv.model.Island;
import cz.cvut.fit.bi.tjv.model.PirateGroup;
import cz.cvut.fit.bi.tjv.model.Ship;
import cz.cvut.fit.bi.tjv.service.IslandService;
import cz.cvut.fit.bi.tjv.service.PirateGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class PirateGroupController {
    @Autowired
    public IslandService islandService;
    @Autowired
    public PirateGroupService pirateGroupService;

    @GetMapping("/pirateGroups/{id}/islands")
    public List<Island> getIslands(@PathVariable String id) {
        return new ArrayList<>(pirateGroupService.find(Integer.parseInt(id)).getIslands());
    }

    @GetMapping("/pirateGroups/{id}/ships")
    public List<Ship> getShips(@PathVariable String id) {
        return new ArrayList<>(pirateGroupService.find(Integer.parseInt(id)).getShips());
    }

    @PostMapping(value = "/islands/{islandId}/pirateGroups")
    public ResponseEntity<PirateGroup> addPirateGroup(@RequestBody PirateGroup pirateGroup, @PathVariable String islandId) {
        islandService.find(Integer.parseInt(islandId)).addPirateGroup(pirateGroup);
        return new ResponseEntity<PirateGroup>(pirateGroup, HttpStatus.CREATED);
    }

    @PutMapping(value = "/pirateGroups/{id}")
    public ResponseEntity <PirateGroup> updatePirateGroup(@RequestBody PirateGroup pirateGroup, @PathVariable String id) {
        pirateGroup.setId_pirategroup(Integer.parseInt(id));
        pirateGroupService.addOrReplace(pirateGroup);
    return new ResponseEntity<PirateGroup>(pirateGroup, HttpStatus.OK);
    }

    @DeleteMapping(value = "/pirateGroups/{id}")
    public ResponseEntity deletePirateGroup(@PathVariable String id) {
        pirateGroupService.deleteGroup(Integer.parseInt(id));
    return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}