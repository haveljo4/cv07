package cz.cvut.fit.bi.tjv.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Ship {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String name;

    public void setSinked(boolean sinked) {
        isSinked = sinked;
    }

    public boolean isSinked() {
        return isSinked;
    }

    private boolean isSinked;

    public Ship() {
    }

    public Ship(String name, boolean isSinked) {
        this.name = name;
        this.isSinked = isSinked;
    }
    public Ship(Integer id, String name, boolean isSinked) {
        this.name = name;
        this.id = id;
        this.isSinked = isSinked;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Ship [id=" + id + ", name=" + name + ", sinked: " + isSinked + "]";
    }
}
