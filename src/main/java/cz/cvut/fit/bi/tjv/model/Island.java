package cz.cvut.fit.bi.tjv.model;
import lombok.Data;
import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Table(name = "island")
@Entity
public class Island {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id_island;

    private String name;

    public Island() {
    }

    public Island(String name) {
        this.name = name;
    }

    public Island(Integer id_island, String name) {
        this.name = name;
        this.id_island = id_island;
    }

    public Integer getId_island() {
        return id_island;
    }

    public void setId_island(Integer id_island) {
        this.id_island = id_island;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPirateGroups(Set<PirateGroup> pirateGroups) {
        this.pirateGroups = pirateGroups;
    }

    public Set<PirateGroup> getPirateGroups() {
        return pirateGroups;
    }
    public void addPirateGroup(final PirateGroup group){
        this.pirateGroups.add(group);
    }

    @ManyToMany (cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinTable(
            name = "island_pirategroup",
            joinColumns = @JoinColumn(referencedColumnName = "id_island"),
            inverseJoinColumns = @JoinColumn(referencedColumnName = "id_pirategroup"))
    private Set<PirateGroup> pirateGroups = new HashSet<>();

    @Override
    public String toString() {
        return "Island [id=" + id_island + ", name=" + name +  "]";
    }
}
