package cz.cvut.fit.bi.tjv.model;

import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Table(name = "pirategroup")
@Entity
public class PirateGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id_pirategroup;

    private String Name;
    private int moneyAmount;

    public PirateGroup() {
    }


    public void setMoneyAmount(int moneyAmount) {
        this.moneyAmount = moneyAmount;
    }

    public void setIslands(Set<Island> islands) {
        this.islands = islands;
    }

    public int getMoneyAmount() {
        return moneyAmount;
    }

    public Set<Island> getIslands() {
        return islands;
    }

    public PirateGroup(String firstName, int moneyAmount) {
        this.Name = firstName;
        this.moneyAmount = moneyAmount;
    }
    public PirateGroup(Integer id_pirategroup, String firstName, int moneyAmount) {
        this.Name = firstName;
        this.id_pirategroup = id_pirategroup;
        this.moneyAmount = moneyAmount;
    }

    public Integer getId_pirategroup() {
        return id_pirategroup;
    }

    public void setId_pirategroup(Integer id_pirategroup) {
        this.id_pirategroup = id_pirategroup;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public int getLastName() {
        return moneyAmount;
    }

    public void setLastName(int moneyAmount) {
        this.moneyAmount = moneyAmount;
    }

    @ManyToMany(mappedBy = "pirateGroups", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private
    Set<Island> islands = new HashSet<>();


    @OneToMany
            (cascade = {CascadeType.MERGE, CascadeType.PERSIST},
                    fetch = FetchType.EAGER)
    private
    Set<Ship> ships = new HashSet<>();


    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        ships.forEach(s -> builder.append(s.getName()));
        return "id=" + id_pirategroup + ", name=" + Name + " money: " + moneyAmount + " ships: " + builder.toString();
    }


    public Set<Ship> getShips() {
        return ships;
    }

    public void setShips(Set<Ship> ships) {
        this.ships = ships;
    }

    public void addShip(Ship ship) {
        this.ships.add(ship);
    }

    public void addIsland(Island island) {
        this.islands.add(island);
    }

}
