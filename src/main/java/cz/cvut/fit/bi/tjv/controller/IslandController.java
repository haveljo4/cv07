package cz.cvut.fit.bi.tjv.controller;

import cz.cvut.fit.bi.tjv.model.Island;
import cz.cvut.fit.bi.tjv.model.PirateGroup;
import cz.cvut.fit.bi.tjv.service.IslandService;
import cz.cvut.fit.bi.tjv.service.PirateGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
public class IslandController {
    @Autowired
    public IslandService islandService;
    @Autowired
    public PirateGroupService pirateGroupService;

    @GetMapping("/islands/{id}/pirateGroups")
    public List<PirateGroup> getPirateGroups(@PathVariable String id) {
        //System.out.println("in get method");
        return new ArrayList<>(islandService.find(Integer.parseInt(id)).getPirateGroups());

    }
    @GetMapping("/islands")
    public List<Island> getIslands() {
        //System.out.println("in get method");
        final List res = new ArrayList<Island>();
         islandService.listAll().forEach(res::add);
         return res;

    }
    @PostMapping(value="/pirateGroups/{pirateGroupId}/islands")
    public ResponseEntity <Island> addIsland(@RequestBody Island island , @PathVariable String pirateGroupId) {
        pirateGroupService.find(Integer.parseInt(pirateGroupId)).addIsland(island);
        return new ResponseEntity<Island>(island, HttpStatus.CREATED);
    }

    @PutMapping(value="/islands/{id}")
    public ResponseEntity <Island> updateIsland(@RequestBody Island island, @PathVariable String id) {
        island.setId_island(Integer.parseInt(id));
        islandService.addOrReplace(island);
        return new ResponseEntity<Island>(island, HttpStatus.OK);
    }

    @DeleteMapping(value="/islands/{id}")
    public ResponseEntity<HttpStatus> deleteIsland(@PathVariable String id) {
        islandService.deleteIsland(Integer.parseInt(id));
        return new ResponseEntity<HttpStatus>(HttpStatus.ACCEPTED);
    }

}
