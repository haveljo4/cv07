package cz.cvut.fit.bi.tjv.dao;

import cz.cvut.fit.bi.tjv.model.PirateGroup;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PirateGroupDao extends CrudRepository<PirateGroup, Integer> {


}