package cz.cvut.fit.bi.tjv.main;

import cz.cvut.fit.bi.tjv.model.Island;
import cz.cvut.fit.bi.tjv.model.PirateGroup;
import cz.cvut.fit.bi.tjv.model.Ship;
import cz.cvut.fit.bi.tjv.service.IslandService;
import cz.cvut.fit.bi.tjv.service.PirateGroupService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DataAccessException;
import cz.cvut.fit.bi.tjv.service.ShipService;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.*;
@EnableJpaRepositories(basePackages="cz.cvut.fit.bi.tjv.dao")
@SpringBootApplication(scanBasePackages = {"cz.cvut.fit.bi.tjv"})
@EntityScan(basePackages = {"cz.cvut.fit.bi.tjv.model"})
public class SpringOrmMain {

    public static void main(String[] args) {
        SpringApplication.run(SpringOrmMain.class, args);

        //Create Spring application context
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");

        //Get service from context. (service's dependency (ProductDAO) is autowired in ProductService)
        ShipService shipService = ctx.getBean(ShipService.class);
        PirateGroupService pirateGroupService = ctx.getBean(PirateGroupService.class);
        IslandService islandService = ctx.getBean(IslandService.class);

        //Do some data operation

        Set<PirateGroup> groups = new HashSet<>();
        PirateGroup goldLovers = new PirateGroup("Gold Lovers" , 1000000);
        PirateGroup  passionatedGunners = new PirateGroup("Passionated Gunners" , 0);
        PirateGroup losers = new PirateGroup("Losers" , 150);


        groups.add(goldLovers);
        groups.add(passionatedGunners);
        groups.add(losers);


        Ship ship1 = new Ship("BlackPearl", false);
        Ship ship2 = new Ship("Nautilus", false);
        Ship ship3 = new Ship("Aurora", false);


        Island ostrov1= new Island("Ostrov1");
        Island madagaskar= new Island("Madagaskar");

        List<Ship> shipList = new ArrayList<>();
        shipList.add(ship1);
        shipList.add(ship2);
        shipList.add(ship3);



//        Set isls = new HashSet<Island>();
//        isls.add(madagaskar);
//        goldLovers.setIslands(isls);
//        losers.setIslands(isls);
//        passionatedGunners.setIslands(isls);

        final Set <Ship> ships = new HashSet<>();
        ships.add(ship1);
        ships.add(ship2);

        losers.setShips(ships);
        goldLovers.setShips(new HashSet<Ship>(){{add(ship3);}});
//        passionatedGunners.setShips(ships);

//        pirateGroupService.add(goldLovers);



//        pirateGroupService.add(goldLovers);
//        pirateGroupService.add(passionatedGunners);
//        pirateGroupService.add(losers);

        madagaskar.setPirateGroups(groups);


        islandService.addOrReplace(ostrov1);
        islandService.addOrReplace(madagaskar);



//        System.out.println("listAll pirateGroups: " + pirateGroupService.listAll());

        System.out.println("listAll islands: " + islandService.listAll());

        System.out.println("listAll ships: " + shipService.listAll());


        //Test transaction rollback (duplicated key)

        try {
            shipService.addAll(Arrays.asList(new Ship("Book", false), new Ship("Soap", false), new Ship("Computer", false)));
        } catch (DataAccessException dataAccessException) {
        }

        //Test element list after rollback
        System.out.println("listAll: " + shipService.listAll());

        ctx.close();

    }
}
